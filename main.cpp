/****************************************************************************
**
** Copyright 2014 by Emotiv. All rights reserved
** Example 1 - EmoStateLogger
** This example demonstrates the use of the core Emotiv API functions.
** It logs all Emotiv detection results for the attached users after
** successfully establishing a connection to Emotiv EmoEngineTM or
** EmoComposerTM
****************************************************************************/
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define SERVER_ADDRESS "localhost"
#define DEFAULT_PORT "54133"
///////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <cstdlib>
#include <stdexcept>
#include <string>
#include <iomanip>
#include <chrono>
#include <thread>
#include <ctime>		/* time_t, time, clock_t, clock, CLOCKS_PER_SEC */

#ifdef _WIN32
    #include <conio.h>
    #include <windows.h>
#endif
#ifdef __linux__
    #include <unistd.h>
#endif

#include "EmoStateDLL.h"
#include "edk.h"
#include "edkErrorCode.h"

#ifdef __linux__
int _kbhit(void);
#endif

struct obj {
	int id;
	int value;
};

obj logEmoState(std::ostream& os, unsigned int userID,
                 EmoStateHandle eState, bool withHeader = false);



//parameter tuning and adding the left, right features
int main(int argc, char** argv) {

	/////////////////////////////

	 WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                    *ptr = NULL,
                    hints;
	char *sendbuf = "{client}";
    char recvbuf[DEFAULT_BUFLEN];
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo(SERVER_ADDRESS, DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, 
            ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            return 1;
        }
		
        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        printf("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }

    // Send an initial buffer

    iResult = send( ConnectSocket, sendbuf, (int)strlen(sendbuf), 0 );
    if (iResult == SOCKET_ERROR) {
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

	///////////////////////////
	EmoEngineEventHandle eEvent			= EE_EmoEngineEventCreate();
	EmoStateHandle eState				= EE_EmoStateCreate();
	unsigned int userID					= 0;
	const unsigned short composerPort	= 1726;
	int option = 0;
	int state  = 0;
	std::string input;
	
	try {
		/*
		if (argc != 2) {
            throw std::runtime_error("Please supply the log file name.\n"
                                     "Usage: EmoStateLogger [log_file_name].");
		}
		*/
        std::cout << "==================================================================="
                  << std::endl;
        std::cout << "Example to show how to log the EmoState from EmoEngine/EmoComposer."
                  << std::endl;
        std::cout << "==================================================================="
                  << std::endl;
        std::cout << "Press '1' to start and connect to the EmoEngine                    "
                  << std::endl;
        std::cout << "Press '2' to connect to the EmoComposer                            "
                  << std::endl;
		std::cout << ">> ";

		std::getline(std::cin, input, '\n');
		option = atoi(input.c_str());

		switch (option) {
			case 1:
			{
				if (EE_EngineConnect() != EDK_OK) {
                    throw std::runtime_error("Emotiv Engine start up failed.");
				}
				break;
			}
			case 2:
			{
				std::cout << "Target IP of EmoComposer? [127.0.0.1] ";
				std::getline(std::cin, input, '\n');

				if (input.empty()) {
					input = std::string("127.0.0.1");
				}

				if (EE_EngineRemoteConnect(input.c_str(), composerPort) != EDK_OK) {
                    std::string errMsg = "Cannot connect to EmoComposer on ["
                                         + input + "]";
                    throw std::runtime_error(errMsg.c_str());
				}
				break;
			}
			default:
                throw std::runtime_error("Invalid option...");
				break;
		}
		std::string user_profile_name;
		std::string command_string;
		std::cout << "Please inter the user profile name : ";
		std::cin >> user_profile_name;
		command_string = "copy " + user_profile_name + " profile.emu";
		system(command_string.c_str());

        std::cout << "Start receiving EmoState! Press any key to stop logging...\n"
                  << std::endl;

		const char* szInputFilename="profile.emu";
		if(EDK_OK==EE_LoadUserProfile(userID,szInputFilename))
			std::cout<<"Load profile successfully!"<<std::endl;
		else
			std::cout<<"Cannot load profile"<<std::endl;

		std::ofstream ofs("state_file");
		bool writeHeader = true;

		time_t now = time(0);
		std::string fileName = "eeg_"+std::to_string(now)+".csv";
		std::ofstream csvFile;
		csvFile.open(fileName);
		csvFile << "time,neutral,zoom in,zoom out,rotate L,rotate R,power\n";
		clock_t clockTime = clock();
		const int sampleIdArraySize = 5;

		while (!_kbhit()) {
			int sampleIdArray[sampleIdArraySize] = {0};
			state = EE_EngineGetNextEvent(eEvent);

			// New event needs to be handled
			if (state == EDK_OK) {
				bool have_user_id=false;
				EE_Event_t eventType = EE_EmoEngineEventGetType(eEvent);
				if(EE_EmoEngineEventGetUserId(eEvent, &userID)==EDK_OK){
					have_user_id=true;
				}
				// Log the EmoState if it has been updated
				if (eventType == EE_EmoStateUpdated && have_user_id) {

					EE_EmoEngineEventGetEmoState(eEvent, eState);
					const float timestamp = ES_GetTimeFromStart(eState);

                    printf("%10.3fs : New EmoState from user %d ...\r",
                           timestamp, userID);
					
					obj sample = logEmoState(ofs, userID, eState, writeHeader);
					std::string sid = std::to_string(sample.id);
					std::string svalue = std::to_string(sample.value);
					std::string sendStr = "{ \"id\":\"" + sid + "\", \"value\":\"" + svalue + "\" }";
					//std::cout << sendStr << std::endl;

					//write sample.id and sample.value into the csv file
					//"time,neutral,zoom in,zoom out,rotate L,rotate R,power"
					sampleIdArray[sample.id] = 1;
					csvFile << ((float)(clock()-clockTime))/CLOCKS_PER_SEC << ",";
					for(int i = 0; i < sampleIdArraySize; i++){
						csvFile << sampleIdArray[i] << ",";
					}
					csvFile << sample.value << "\n";
					///////////////////////////
					iResult = send( ConnectSocket, sendStr.c_str(), (int)strlen(sendStr.c_str()), 0 );
    if (iResult == SOCKET_ERROR) {
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }
					///////////////////////////
					//std::this_thread::sleep_for(std::chrono::milliseconds(2000));
					writeHeader = false;
				}
			}
			else if (state != EDK_NO_EVENT) {
                std::cout << "Internal error in Emotiv Engine!"
                          << std::endl;
				break;
			}

#ifdef _WIN32
			Sleep(1);
#endif
#ifdef __linux__
            sleep(1);
#endif
		}

		ofs.close();
		csvFile.close();
	}
	catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::cout << "Press any key to exit..." << std::endl;
		getchar();
	}

	EE_EngineDisconnect();
	EE_EmoStateFree(eState);
	EE_EmoEngineEventFree(eEvent);

	return 0;
}


obj logEmoState(std::ostream& os, unsigned int userID,
                 EmoStateHandle eState, bool withHeader) {
	obj sample;
	// Create the top header
	if (withHeader) {
		os << std::setw(20)<<"Time";
		os << std::setw(22)<<"UserID";
		os << std::setw(27)<<"Signal Status";
		//os << "Blink,";
		//os << "Wink Left,";
		//os << "Wink Right,";
		//os << "Look Left,";
		//os << "Look Right,";
		//os << "Eyebrow,";
		//os << "Furrow,";
		//os << "Smile,";
		//os << "Clench,";
		//os << "Smirk Left,";
		//os << "Smirk Right,";
		//os << "Laugh,";
		//os << "Short Term Excitement,";
		//os << "Long Term Excitement,";
		//os << "Engagement/Boredom,";
		os << std::setw(28)<<"Cognitiv Active";
		os << std::setw(28)<<"Cognitiv Action";
		os << std::setw(27)<<"Cognitiv Power";
		os << std::endl;
	}

	EE_SignalStrength_enum EE_SignalStrength_t=ES_GetWirelessSignalStatus(eState);
	std::string sig_strength;
	if(EE_SignalStrength_t==NO_SIGNAL)
		sig_strength="NO_SIGNAL";
	else if(EE_SignalStrength_t==BAD_SIGNAL)
		sig_strength="BAD_SIGNAL";
	else if(EE_SignalStrength_t==GOOD_SIGNAL)
		sig_strength="GOOD_SIGNAL";
	else
		sig_strength="OTHERS";

	// Log the time stamp and user ID
	os << std::setw(20)<<ES_GetTimeFromStart(eState);
	os << std::setw(22)<<userID;
	os << std::setw(27)<<sig_strength;

	// Expressiv Suite results
	//os << ES_ExpressivIsBlink(eState) << ",";
	//os << ES_ExpressivIsLeftWink(eState) << ",";
	//os << ES_ExpressivIsRightWink(eState) << ",";

	//os << ES_ExpressivIsLookingLeft(eState) << ",";
	//os << ES_ExpressivIsLookingRight(eState) << ",";
	/*
	std::map<EE_ExpressivAlgo_t, float> expressivStates;

	EE_ExpressivAlgo_t upperFaceAction = ES_ExpressivGetUpperFaceAction(eState);
	float			   upperFacePower  = ES_ExpressivGetUpperFaceActionPower(eState);

	EE_ExpressivAlgo_t lowerFaceAction = ES_ExpressivGetLowerFaceAction(eState);
	float			   lowerFacePower  = ES_ExpressivGetLowerFaceActionPower(eState);

	expressivStates[ upperFaceAction ] = upperFacePower;
	expressivStates[ lowerFaceAction ] = lowerFacePower;
	*/
	//os << expressivStates[ EXP_EYEBROW     ] << ","; // eyebrow
	//os << expressivStates[ EXP_FURROW      ] << ","; // furrow
	//os << expressivStates[ EXP_SMILE       ] << ","; // smile
	//os << expressivStates[ EXP_CLENCH      ] << ","; // clench
	//os << expressivStates[ EXP_SMIRK_LEFT  ] << ","; // smirk left
	//os << expressivStates[ EXP_SMIRK_RIGHT ] << ","; // smirk right
	//os << expressivStates[ EXP_LAUGH       ] << ","; // laugh

	// Affectiv Suite results
	//os << ES_AffectivGetExcitementShortTermScore(eState) << ",";
	//os << ES_AffectivGetExcitementLongTermScore(eState) << ",";

	//os << ES_AffectivGetEngagementBoredomScore(eState) << ",";

	// Cognitiv Suite results
	std::string cognitiv_action;
	EE_CognitivAction_enum EE_CognitivAction_t=ES_CognitivGetCurrentAction(eState);
	if(EE_CognitivAction_t==COG_PULL)
		cognitiv_action="PULL";
	else if(EE_CognitivAction_t==COG_PUSH)
		cognitiv_action="PUSH";
	else if(EE_CognitivAction_t==COG_LIFT)
		cognitiv_action="LIFT";
	else if(EE_CognitivAction_t==COG_DROP)
		cognitiv_action="DROP";
	else if(EE_CognitivAction_t==COG_LEFT)
		cognitiv_action="LEFT";
	else if(EE_CognitivAction_t==COG_RIGHT)
		cognitiv_action="RIGHT";
	else if(EE_CognitivAction_t==COG_ROTATE_LEFT)
		cognitiv_action="ROTATE_LEFT";
	else if(EE_CognitivAction_t==COG_ROTATE_RIGHT)
		cognitiv_action="ROTATE_RIGHT";
	else if(EE_CognitivAction_t==COG_ROTATE_CLOCKWISE)
		cognitiv_action="ROTATE_CLOCKWISE";
	else if(EE_CognitivAction_t==COG_ROTATE_COUNTER_CLOCKWISE)
		cognitiv_action="ROTATE_COUNTER_CLOCKWISE";
	else if(EE_CognitivAction_t==COG_ROTATE_FORWARDS)
		cognitiv_action="ROTATE_FORWARDS";
	else if(EE_CognitivAction_t==COG_ROTATE_REVERSE)
		cognitiv_action="ROTATE_REVERSE";
	else if(EE_CognitivAction_t==COG_DISAPPEAR)
		cognitiv_action="DISAPPEAR";
	else if(EE_CognitivAction_t==COG_NEUTRAL)
		cognitiv_action="NEUTRAL";
	else
		cognitiv_action="OTHERS";
	if(EE_CognitivAction_t==COG_PULL){
		sample.id=1;
	}
	else if(EE_CognitivAction_t==COG_PUSH){
		sample.id=2;
	}
	else if (EE_CognitivAction_t==COG_ROTATE_LEFT){
		sample.id=3;
	}
	else if(EE_CognitivAction_t==COG_ROTATE_RIGHT){
		sample.id=4;
	}
	else{
		sample.id=0;
	}
	float power=ES_CognitivGetCurrentActionPower(eState);
	std::cout << power <<std::endl;
	sample.value=(int)(power*100);
	int active=ES_CognitivIsActive(eState);
	std::string act;
	if(active==1)
		act="active";
	else
		act="not active";
	os <<std::setw(28)<<act;
	os << std::setw(28)<<cognitiv_action;
	//os << static_cast<int>(ES_CognitivGetCurrentAction(eState)) << ",";
	os << std::setw(27)<<power;

	os << std::endl;
	return sample;
}

#ifdef __linux__
int _kbhit(void)
{
    struct timeval tv;
    fd_set read_fd;

    tv.tv_sec=0;
    tv.tv_usec=0;

    FD_ZERO(&read_fd);
    FD_SET(0,&read_fd);

    if(select(1, &read_fd,NULL, NULL, &tv) == -1)
    return 0;

    if(FD_ISSET(0,&read_fd))
        return 1;

    return 0;
}
#endif