(	--background thread tests
--resetMaxFile #noPrompt; clearListener()

socket = dotNetObject "System.Net.Sockets.Socket" (dotnetclass "System.Net.Sockets.AddressFamily").InterNetwork (dotnetclass "System.Net.Sockets.SocketType").Stream (dotnetclass "System.Net.Sockets.ProtocolType").Tcp
 
socket.connect "localhost" 54134
 
buf = DotNetObject "System.Byte[]" 1

fn bkgWrkr =
(
	
	try
	(
		prev_command = 0
		while socket.Connected == true do
		(
			socket.Receive buf
			command = prev_command
			command2 = buf.GetValue 0
			prev_command = command2
			if(command == command2) then
			(
				case command of
				(
					49: zoomIn()
					50: zoomOut()
					51: left()
					52: right()
					48: print "neutral"
					default: print "undefined"
				)
				prev_command = 0
			)
			else
			(
				print "no two consecutive same commands"
			)
		)
	)
	catch
	(
		-- do nothing
	)
	
) --end bkgWrkr

fn zoomIn =
(
	max izoom in
	print "zoom in"
)

fn zoomOut =
(
	max izoom out
	print "zoom out"
)

fn left =
(
	theAxis = (viewport.getTM()).row3
	for i = 1 to 15 do
	(
		viewport.rotate (quat 2 theAxis)
		completeredraw()
	)
	print "rotate left"
)

fn right =
(
	theAxis = (viewport.getTM()).row3
	for i = 1 to 15 do
	(
		viewport.rotate (quat -2 theAxis)
		completeredraw()
	)
	print "rotate right"
)

MainThread = dotnetobject "CSharpUtilities.SynchronizingBackgroundWorker"
MainThread.WorkerSupportsCancellation = true	
dotNet.addEventHandler MainThread "DoWork" bkgWrkr
MainThread.RunWorkerAsync()
print "Script start running..."; gc()


--for i=0 to (buf.length-1) do(
	--print (buf.GetValue i)
	--sleep 1
--)
--close socket
--socket.close()

)