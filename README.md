# Synopsis

This is the client part of the EEG control project. In this repository, we define and implement our own EMOTIV client using
[EMOTIV API](https://github.com/Emotiv/community-sdk) and [Windows sockets API(WSA)](https://en.wikipedia.org/wiki/Winsock)
to transmit mental command status to the server-side application. For 3dsMax.ms and rhino.py, they are designed to be the client-side applications to receive the
mental command status and do the correct operations on 3D Computer-Aided Design(CAD) software such as 3ds Max and Rhino in real-time.

# Motivation

The main program, EMOTIV client utilizes [EMOTIV API](https://github.com/Emotiv/community-sdk) and [WSA](https://en.wikipedia.org/wiki/Winsock) to provide an
internet-based mental command retrieval method. Making manipulation of CAD software more intuitive by just thinking. Get rid of all different shortcuts on
CAD software and make design easier.

# Installation

To compile the main program and use the [EMOTIV API](https://github.com/Emotiv/community-sdk), we need to install Visual Studio 2008/2010/2012 firstly.
After setting the includes, static links and dynamic links on Visual Studio, we can finally compile our source files to the executable. Note that you should
choose the right platform when you build the project.

The 3dsMax.ms and rhino.py are written by scripting languages, so there is no need to compile the source file. Just run them on your CAD softwares.

# Tests

Please reference to your executable after compilation.
```bash
emotiv-client.exe
```

# Contributor

Jason Chen

