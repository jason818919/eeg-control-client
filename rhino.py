import rhinoscriptsyntax as rs
import thread
import socket
import json

TCP_HOST = 'localhost'
TCP_PORT = 54135
BUFFER_SIZE = 1

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_HOST, TCP_PORT))

def emotiv_control():
	# connect to server and receive data continuously
	print "Initialization completes. Waiting for command..."
	prev_message = '0'
	while 1:
		message = prev_message
		message2 = s.recv(BUFFER_SIZE)
		prev_message = message2
		# judge and draw
		if message == message2:
			if message=='1':
				camera_position=rs.ViewCamera()
				camera_position=camera_position*0.8
				rs.ViewCamera(camera_location=camera_position) # zoom in
				print "zoom in"
			elif message=='2':
				camera_position=rs.ViewCamera()
				camera_position=camera_position*1.2
				rs.ViewCamera(camera_location=camera_position) # zoom out
				print "zoom out"
			elif message=='3':
				rs.RotateView(direction=1) # rotate left
				print "rotate left"
			elif message=='4':
				rs.RotateView(direction=0) # rotate right
				print "rotate right"
			else:
				print "undefined or neutral"
			prev_message = '0'
			rs.Redraw()
		else:
			print "no two consecutive same commands"
thread.start_new_thread(emotiv_control,());
